import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    listaFiltro: []
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false,
      listaFiltro: await listNotes()
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editNote = async (index) => {
    if (index > -1) {
      this.setState({
        text: this.state.notes[index].text,
        new: true,
        indexToSave: index
      })
    }
  }

  delNote = async (note) => {
    if(note > -1) {
      this.state.notes.splice(note, 1);
      console.log(await listNotes())
      this.setState({
        notes: await listNotes(),
        listaFiltro: await listNotes()
      })
      console.log(this.state.notes)
    }
  }

  filterNote = async (searchTerm) => {

    this.setState({
      listaFiltro: this.state.notes.filter(nota => nota.text.toLowerCase().search(searchTerm.target.value.toLowerCase()) !== -1)
    })

  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: '',
      listaFiltro: await listNotes
    })
  }
  
  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' onChange={this.filterNote} placeholder='Procurar...' />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            { this.state.listaFiltro.map((note, index) => (
              <div className='item' key={note.id}>
                <input onChange={this.editText} className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={() => {this.delNote(index)}}>Excluir</button>) }
                  { note.id && (<button onClick={() => {this.editNote(index)}}>Editar</button>) }
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
