const result = [
	{ text: 'Lavar o carro', id: 4242424 },
	{ text: 'Dar vacina na Alice', id: 232323 }
]

const listNotes = () => {
	
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(result)
		}, 1)
	})
}

const saveNotes = (text, index) => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			if (index !== -1) {
				result[index].text = text
			} else {
				result.unshift({
					text: text,
					id: Math.random().toString(36).substr(2, 16)
				})
			}
			resolve()
		}, 100)
	})
}

module.exports = {
	listNotes,
	saveNotes
}